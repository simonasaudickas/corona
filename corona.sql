drop table if exists corona.virus_spread;
create table corona.virus_spread as
select 
			vc.*, 
			coalesce (avg(pl.latitude),avg(pl1.latitude ) ) as latitude , 
			coalesce (avg(pl.longitude),avg(pl1.longitude )) as longitude
from corona.virus_cases vc 
left join assets.postcode_lookup pl 
			on vc.gss_cd = pl.district_code 
			and pl.in_use = 'Yes' 
left join assets.postcode_lookup pl1
		on vc.gss_cd = pl1.county_code 
		and pl1.in_use = 'Yes'
group by 1,2,3,4,5;


UPDATE 
   corona.virus_spread
SET 
   gss_nm = 'Bournemouth'
WHERE 
   gss_nm='Bournemouth, Christchurch and Poole';

UPDATE 
   corona.virus_spread
SET 
   longitude = '-1.85864'::numeric,
   latitude ='50.74486'::numeric
WHERE 
   gss_nm='Bournemouth'
   and latitude is null 
   and longitude is null;

UPDATE 
   corona.virus_spread
SET 
   longitude = '-2.41467'::numeric,
   latitude ='50.79697'::numeric
WHERE 
   gss_nm='Dorset'
   and latitude is null 
   and longitude is null;
  
   
   drop table if exists corona.virus_spread_day_diff;
  create table corona.virus_spread_day_diff as
   select gss_nm, 
   					total_cases,
   					date,
   					lag(total_cases) over (partition by gss_nm order by date asc) as previous_day_cases,
   					total_cases - lag(total_cases) over (partition by gss_nm order by date asc) as day_diff, 
   					longitude,
   					latitude
from corona.virus_spread vs ;
   
--Indexind the data---
drop table if exists corona.virus_spread_index;
create table corona.virus_spread_index as
select 
			vc.*, 
			--coalesce (avg(pl.latitude),avg(pl1.latitude ) ) as latitude , 
			--coalesce (avg(pl.longitude),avg(pl1.longitude )) as longitude,
			coalesce (sum(pl.population),	sum(pl1.population )) as area_pop,
			1.00*total_cases/(coalesce (sum(pl.population),sum(pl1.population ))/10000) as infection_index
from corona.virus_spread vc 
left join assets.postcode_lookup pl 
			on vc.gss_cd = pl.district_code 
			and pl.in_use = 'Yes' 
left join assets.postcode_lookup pl1
		on vc.gss_cd = pl1.county_code 
		and pl1.in_use = 'Yes'
group by 1,2,3,4,5,6,7;

drop table if exists corona.index_calcs;
create table corona.index_calcs as
with prep as (select sum(total_cases) as total_cases, sum(area_pop), 
				1.00*((sum(total_cases)/(sum(area_pop)/10000))) as country_dif,
				 date 
from corona.virus_spread_index
group by 4
order by 4 asc)
,country as (
select country_dif,
lag (country_dif) over (order by date),
((country_dif-(lag(country_dif) over(order by date)))/lag(country_dif) over(order by date))*100 as national_pct_change,
date
from prep),
all_data as (select gss_nm, vsi.date, infection_index as current_day_index,
				lag(infection_index) over (partition by gss_nm order by vsi.date asc) as prev_day_index,
				case when(lag(infection_index) over (partition by gss_nm order by vsi.date asc))=0 then 0 
				else 
				((infection_index-(lag(infection_index) over (partition by gss_nm order by vsi.date asc)))/lag(infection_index) over (partition by gss_nm order by vsi.date asc))*100 
				end as area_pct_change,
				c.national_pct_change			
from corona.virus_spread_index vsi 
left join country c on vsi.date=c.date
order by date asc)
select * , 
				-( national_pct_change-area_pct_change) as total_pct_change
from all_data;



drop table if exists corona.world_stats_coord;
create table corona.world_stats_coord as
with prep as (select ws.country_name,
				replace(cases, ',','') as cases,
				replace(deaths,',','') as deaths,
				replace(region, ',','') as region,
				replace(total_recovered, ',','') as total_recovered,
				replace(new_deaths, ',','') as new_deaths,
				replace(new_cases,',','') as  new_cases,
				replace(serious_critical, ',','') as serious_critical,
				date
				from corona.world_stats ws )
select 
ws.country_name,
				replace(cases, '"','')::int as cases,
				replace(deaths, '"','')::int as deaths,
				replace(region,  '"','') as region,
				replace(total_recovered, '"','')::int as total_recovered,
				replace(new_deaths,  '"','')::int as new_deaths,
				replace(new_cases, '"','')::int as  new_cases,
				replace(serious_critical, '"','')::int as serious_critical,
				"date"::date,
				cc.country_index,
				cc.latitude,
				cc.longitude
from prep ws
left join assets.country_coordinates  cc 
			on ws.country_name=cc.country_name ;


--==Working with indexing ==--
drop table if exists corona.virus_spread_index_comp;
create table corona.virus_spread_index_comp as 
with prep as (select date, avg(total_cases) as avg_cases, avg(infection_index) as index_avg from corona.virus_spread_index vsi group by "date" )
select 
				vsi.*, 
				b.avg_cases,
				vsi.total_cases-b.avg_cases as comp_to_pop,
				vsi.infection_index- b.index_avg as index_comp_to_pop
from corona.virus_spread_index vsi
left join prep b on vsi.date=b.date;
 

drop table if exists corona.index_comp;
create table corona.index_comp as 
with prep as (
						select gss_nm,date, infection_index, lead(infection_index )over(partition by gss_nm order by date asc) as index_diff
						from corona.virus_spread_index vsi group by 1,2, 3 )
select vsi.gss_nm,
				vsi.date,
				avg(total_cases) as avg_cases, 
				avg(vsi.infection_index) as index_avg,
				stddev(vsi.infection_index) over (order by vsi.date asc)  as index_of, 
				p.index_diff,
				p.index_diff-stddev(vsi.infection_index) over (order by vsi.date asc) as perf,
				case when p.index_diff-stddev(vsi.infection_index) over (order by vsi.date asc) >0 then 'over index'
				else 'under index'
				end as index_flag
from corona.virus_spread_index vsi 
left join prep p on vsi.gss_nm=p.gss_nm and p.date=vsi.date
group by vsi.gss_nm, vsi.infection_index, vsi.date,6;



