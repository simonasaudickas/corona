
# coding: utf-8

# In[2]:


import requests
import pandas as pd
import numpy as np
import psycopg2 as pg
from sqlalchemy import create_engine
import io
import datetime


# In[29]:


conn_string= "dbname='dwh' user='masteruser' host='dev-analytics.cnimcclfnoqr.eu-west-1.rds.amazonaws.com'"
try:
	conn=pg.connect(conn_string)
	print ("Successful Connection")
except Exception as e:
	print ("Connection Error: " +str(e))
conn.autocommit=True


# In[30]:


url = "https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php"

headers = {
    'x-rapidapi-host': "coronavirus-monitor.p.rapidapi.com",
    'x-rapidapi-key': "a705f8fcafmsh9a2dc799e683468p18d984jsn329cf1073e42"
    }

response = requests.request("GET", url, headers=headers)

res=response.content.decode('utf-8')
print(response.content)


# In[31]:



results=eval(res)
main_df=pd.DataFrame(results['countries_stat'])
main_df


# In[32]:


main_df.country_name.replace(['UAE', 'UK', 'USA'], ['United Arab Emirates','United Kingdom', 'United States'], inplace=True)
main_df.loc[main_df['country_name']== 'United Kingdom']


# In[33]:





# In[34]:


today = datetime.date.today()
yesterday = today - datetime.timedelta(days = 1)
yesterday.isoformat()
main_df['date']=yesterday
dictionary={',':''}
main_df.replace(dictionary, regex=True, inplace=True)
main_df


# In[38]:


#Extracting just the needed fields
main_df=main_df[['country_name',
                 'cases',
                 'deaths',
                 'region',
                 'total_recovered',
                 'new_deaths',
                 'new_cases',
                 'serious_critical',
                'date']]
main_df


# In[39]:


#Replacing the non-integer value manually and checking if there are no more non-integer values
main_df=main_df.replace({'total_recovered': {'N/A': 344}})
main_df.loc[main_df['total_recovered']== 'N/A']


# In[40]:


#Appending the dataframe data to the database table that has been created
engine = create_engine('postgresql+psycopg2://masteruser:W=!QnL8XbXqakMd=@dev-analytics.cnimcclfnoqr.eu-west-1.rds.amazonaws.com:5432/dwh')
main_df.to_sql('world_stats', engine, if_exists='append', schema='corona',index=False) #truncates the table
conn = engine.raw_connection()
conn.commit()

