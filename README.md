The project is created to track the COVID-19 virus statistics.

The data is stored under the the Dev-analytics DB (dwh)->Schema: corona

The table corona.virus_cases is updated manualy by Uploading the data downloaded from the page https://coronavirus.data.gov.uk/#local-authorities

World statistics is retrieved via an API: "https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php". We use the python code to retrieve daily data(API does not have historical data so the process is run daily) and the data is pushed to the table: corona.world_stats

After the data is uploaded to the database, the SQL code is run.

The results are displayed at: https://qei.beyondanalysis.net/qei/dashboard/corona/
